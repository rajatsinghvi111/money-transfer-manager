package revolut.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Account {
	
	private Long accountNo;
	private Double balance;
	
	public Account(Long accountNo, Double balance) {
		this.accountNo = accountNo;
		this.balance = balance;
	}
	
	public Account creditAmount(Double amount) {
			this.balance = getBalance() + amount;
			return this;
	}
	
	public Account debitAmount(Double amount) {
			this.balance = getBalance() - amount;
			return this;
	} 
	
	public Double getBalance() {
		return this.balance;
	}
}
