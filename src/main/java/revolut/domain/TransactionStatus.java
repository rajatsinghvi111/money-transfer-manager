package revolut.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TransactionStatus {
	
	SUCCESS(Constants.SUCCESS),
	FAILED(Constants.FAILED),
	PENDING(Constants.PENDING);
	
	private String transactionType;

	private static class Constants {
        public static final String SUCCESS = "Success";
        public static final String FAILED = "Failed";
        public static final String PENDING = "Pending";
    }
}
