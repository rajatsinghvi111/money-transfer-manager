package revolut;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;

import org.jboss.resteasy.plugins.server.sun.http.HttpContextBuilder;

import com.sun.net.httpserver.HttpServer;

import revolut.constant.Constants;
import revolut.services.AccountService;
import revolut.services.TransactionService;

public class Application {
	
	public static void main(String args[]) throws IOException {
		Migration.executeMigration();
		
		HttpServer server = HttpServer.create(new InetSocketAddress(8080), 1);
		HttpContextBuilder contextBuilder = new HttpContextBuilder();
		contextBuilder.getDeployment().getActualResourceClasses()
				.addAll(Arrays.asList(AccountService.class, TransactionService.class));
		contextBuilder.bind(server);
		server.start();
		System.out.print("Application Started Successfully");
		
	}
	
	 public static Connection getDBConnection() {
	        Connection dbConnection = null;
	        try {
	            Class.forName(Constants.DataBaseProperties.DB_DRIVER);
	        } catch (ClassNotFoundException e) {
	            System.out.println(e.getMessage());
	        }
	        try {
			dbConnection = DriverManager.getConnection(Constants.DataBaseProperties.DB_CONNECTION,
					Constants.DataBaseProperties.DB_USER, Constants.DataBaseProperties.DB_PASSWORD);
	            return dbConnection;
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
	        return dbConnection;
	    }
}
