package revolut.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import revolut.constant.Constants;
import revolut.domain.Account;
import revolut.domain.TransactionStatus;
import revolut.domain.Transfer;
import revolut.exception.TransactionException;

@Path("/transfer")
@Consumes({ "application/json"}) 
@Produces({ "application/json"})
public class TransactionService {
	
	private AccountService accountService;
	
	public TransactionService() {
		accountService = new AccountService();
	}
	
	@POST
	public synchronized TransactionStatus transfer(Transfer transfer) {
			boolean debit = false;
			boolean credit = false;
			Account senderAccount = null;
			Account receiverAccount = null;
			try {
				senderAccount = accountService.fetch(transfer.getSenderAccount());
				receiverAccount = accountService.fetch(transfer.getReceiverAccount());

				validateTransaction(senderAccount, receiverAccount, transfer);
				
				Account updatedSenderAccount = senderAccount.debitAmount(transfer.getAmount());
				Account updatedReceiverAccount = receiverAccount.creditAmount(transfer.getAmount());
				
				debit = accountService.update(updatedSenderAccount);
				credit = accountService.update(updatedReceiverAccount);
			} catch (Exception e) {
				return TransactionStatus.FAILED;
			} finally {
				if (debit && !credit) {
					accountService.update(senderAccount.creditAmount(transfer.getAmount()));
				}
			}
			return TransactionStatus.SUCCESS;
	}

	
		
	private void validateTransaction(Account senderAccount, Account receiverAccount, Transfer transfer) {
		if (transfer.getAmount() <= 0) {
			throw new TransactionException(Constants.AMOUNT_NOT_VALID);
		} else if (senderAccount.getAccountNo() == null
				|| receiverAccount.getAccountNo() == null) {
			throw new TransactionException(Constants.ACCOUNT_NOT_EXISTS);
		} else if (transfer.getSenderAccount() == transfer.getReceiverAccount()) {
			throw new TransactionException(Constants.RECEIVER_ACCOUNT_NOT_VALID);
		} else if (senderAccount.getBalance() < transfer.getAmount()) {
			throw new TransactionException(Constants.NOT_SUFFICIENT_BALANCE);
		}
	}
}
