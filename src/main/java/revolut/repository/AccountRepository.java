package revolut.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import revolut.Application;
import revolut.constant.Constants;
import revolut.domain.Account;
import revolut.exception.AccountException;

public class AccountRepository {
	
	private static final String insertQuery = "insert into account(account_no, balance) values(?,?)";
	private static final String selectQuery = "select * from account where account_no = ?";
	private static final String updateQuery = "update account set balance = ? where account_no = ?";
	private static final String selectAllQuery = "select * from account";
	
	public void createAccount(Account account) throws SQLException {
		Connection connection = Application.getDBConnection();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
			preparedStatement.setLong(1, account.getAccountNo());
			preparedStatement.setDouble(2, account.getBalance());
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e) {
			throw new AccountException(Constants.ACCOUNT_NOT_CREATED);
		} finally {
			connection.close();
		}
	}
	
	public Account getAccountById(Long accountId) throws SQLException {
		Connection connection = Application.getDBConnection();
		Account account = new Account();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
			preparedStatement.setLong(1, accountId);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				account.setAccountNo(rs.getLong("account_no"));
				account.setBalance(rs.getDouble("balance"));
			}
			
			if(account.getAccountNo() == null) {
				throw new AccountException(Constants.ACCOUNT_NOT_EXISTS);
			}
			preparedStatement.close();
		} catch (SQLException e) {
			throw new AccountException(Constants.ACCOUNT_NOT_FETCH);
		} finally {
			connection.close();
		}
		return account;
	}
	
	public List<Account> getAllAccounts() throws SQLException {
		Connection connection = Application.getDBConnection();
		List<Account> accounts = new ArrayList<>();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(selectAllQuery);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				Account account = new Account(rs.getLong("account_no"), rs.getDouble("balance"));
				accounts.add(account);
			}
			preparedStatement.close();
		} catch (SQLException e) {
			throw new AccountException(Constants.ACCOUNT_NOT_FETCH);
		} finally {
			connection.close();
		}
		return accounts;
	}
	
	public int updateAccountSpend(Account account) throws SQLException {
		int count = 0;
		Connection connection = Application.getDBConnection();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(updateQuery);
			preparedStatement.setDouble(1, account.getBalance());
			preparedStatement.setLong(2, account.getAccountNo());
			count = preparedStatement.executeUpdate();
			preparedStatement.close();
			return count;
		} catch (SQLException e) {
			throw new AccountException(Constants.BALANCE_UPDATE_ERROR);
		} finally {
			connection.close();
		}
	}
}
