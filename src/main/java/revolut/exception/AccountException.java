package revolut.exception;

public class AccountException extends RuntimeException {
	private static final long serialVersionUID = 643658732232841118L;

	public AccountException(String reason) {
		super(reason);
	}
}
