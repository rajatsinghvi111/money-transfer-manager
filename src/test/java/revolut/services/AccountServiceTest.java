package revolut.services;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import revolut.Application;
import revolut.Migration;
import revolut.constant.Constants;
import revolut.domain.Account;
import revolut.exception.AccountException;

@RunWith(JUnit4.class)
public class AccountServiceTest {
	
	private static AccountService accountService = new AccountService();
	
	private static Account account1Actual;
	private static Account account2Actual;
	
	@BeforeClass
	public static void DBSetup() {
		Migration.executeMigration();
	}
	
	@Test
	public void shouldGetAccountDetailsById() {
		createAccount();
		Account account = accountService.fetch(account1Actual.getAccountNo());
		assertEquals(account.getBalance(), account1Actual.getBalance());
	}
	
	@Test
	public void shouldThrowAccountNotExistsException() {
		try {
			createAccount();
			Account account = accountService.fetch(1l);
		} catch (AccountException exception) {
			assertEquals(Constants.ACCOUNT_NOT_EXISTS, exception.getMessage());
		}
	}
	
	@Test
	public void shouldFetchAllAccountDetails() {
		createAccount();
		List<Account> accounts = accountService.fetchAll();
		assertEquals(accounts.size(), 2);
	}
	
	@Test
	public void shouldThrowExceptionWhenUpdateAccountBalance() {
		Account account = new Account(1l, 20000.0);
		try {
			accountService.update(account);
		} catch (AccountException exception) {
			assertEquals(Constants.ACCOUNT_NOT_EXISTS, exception.getMessage());
		}
	}

	@Test
	public void shouldUpdateAccountBalance() {
		Account account1Actual = new Account(11113l, 10000.0);
		accountService.create(account1Actual);
		Account account1Updated = new Account(11113l, 20000.0);
		Boolean isUpdated = accountService.update(account1Updated);
		Account expectedAccount = accountService.fetch(11113l);
		assertEquals(expectedAccount.getBalance(), new Double(20000.0));
		assertEquals(isUpdated, true);
	}
	
	private void createAccount() {
		account1Actual = new Account(11111l, 10000.0);
		account2Actual = new Account(11112l, 20000.0);
		
		accountService.create(account1Actual);
		accountService.create(account2Actual);
	}
	
	@After
	public void tearDown() {
	    try {
	        clearDatabase();
	    } catch (Exception e) {
	        
	    }
	}

	public void clearDatabase() throws SQLException {
	    Connection connection = Application.getDBConnection();
	    Statement s = connection.createStatement();

	    // Disable FK
	    s.execute("SET REFERENTIAL_INTEGRITY FALSE");

	    // Find all tables and truncate them
	    Set<String> tables = new HashSet();
	    ResultSet rs = s.executeQuery("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES  where TABLE_SCHEMA='PUBLIC'");
	    while (rs.next()) {
	        tables.add(rs.getString(1));
	    }
	    rs.close();
	    for (String table : tables) {
	        s.executeUpdate("TRUNCATE TABLE " + table);
	    }

	    // Idem for sequences
	    Set<String> sequences = new HashSet<String>();
	    rs = s.executeQuery("SELECT SEQUENCE_NAME FROM INFORMATION_SCHEMA.SEQUENCES WHERE SEQUENCE_SCHEMA='PUBLIC'");
	    while (rs.next()) {
	        sequences.add(rs.getString(1));
	    }
	    rs.close();
	    for (String seq : sequences) {
	        s.executeUpdate("ALTER SEQUENCE " + seq + " RESTART WITH 1");
	    }

	    // Enable FK
	    s.execute("SET REFERENTIAL_INTEGRITY TRUE");
	    s.close();
	    connection.close();
	}
	

}
