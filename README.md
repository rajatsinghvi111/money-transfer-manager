# Money Transfer Manager

Rest Api to transfer money between accounts. Currency is not supported for now.

# Technologies Used

	• Java8
	• H2 in-memory-database
	• Flyway
	• Lombok
	• resteasy
	• Junit4

# Application Setup and Run
  
  # SetUp code and Build

   clone the project repo `git clone https://rajatsinghvi111@bitbucket.org/rajatsinghvi111/money-transfer-manager.git`

   Build the application `gradle clean build`

   Create Jar with all dependencies  `gradle fatJar`

  # Start the Application
   
   first execute `gradle fatJar` before starting application

   java -jar build/libs/MoneyTransactionManager-all.jar 
